$(".card button").each(function() {
  $(this).click(function (e) {
    $(this).parent().find("img").css("opacity", "0.65");
  })
});

$(".tab-horizontal .tab-menu-link").each(function() {
  $(this).click(function (e) {
    $(this).css("background", "#9E6E86");
  })
});

$(".cards-list .card button").each(function() {
  $(this).hover(function (e) {
    $(this).parent().toggleClass("hover");
  })
});

$(".slideshow-nav button").each(function() {
  $(this).hover(function (e) {
    $(this).toggleClass("hover");
  })
});
